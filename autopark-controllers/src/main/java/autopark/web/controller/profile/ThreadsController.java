package autopark.web.controller.profile;

import autopark.dto.UserDTO;
import autopark.service.IUserService;
import autopark.utils.FolderProcessor;
import autopark.web.controller.Constants;
import autopark.web.utils.WebHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.ForkJoinPool;

@Controller
public class ThreadsController {

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;


    @Autowired
    private WebHelper webHelper;


    private String phone = "1112223330";

    @RequestMapping(value = "/testthreads")
    public String handleSignUpPost(ModelMap modelMap, HttpServletRequest request) {
        UserDTO user1 = new UserDTO();
        String password = "123456";

        user1.setPhone(phone);
        user1.setBirthday(new Date());
        user1.setEmail("user1@test.ru");
        user1.setName("USER1");
        user1.setPassword(password);

        UserDTO user2 = new UserDTO();
        user2.setPhone(phone);
        user2.setBirthday(new Date());
        user2.setEmail("user2@test.ru");
        user2.setName("USER2");
        user2.setPassword(password);

        UserDTO user3 = new UserDTO();
        user3.setPhone(phone);
        user3.setBirthday(new Date());
        user3.setEmail("user3@test.ru");
        user3.setName("USER3");
        user3.setPassword(password);

        UserDTO user4 = new UserDTO();
        user4.setPhone(phone);
        user4.setBirthday(new Date());
        user4.setEmail("user4@test.ru");
        user4.setName("USER4");
        user4.setPassword(password);

        UserDTO user5 = new UserDTO();
        user5.setPhone(phone);
        user5.setBirthday(new Date());
        user5.setEmail("user5@test.ru");
        user5.setName("USER5");
        user5.setPassword(password);

        UserDTO user6 = new UserDTO();
        user6.setPhone(phone);
        user6.setBirthday(new Date());
        user6.setEmail("user6@test.ru");
        user6.setName("USER6");
        user6.setPassword(password);

        UserDTO user7 = new UserDTO();
        user7.setPhone(phone);
        user7.setBirthday(new Date());
        user7.setEmail("user7@test.ru");
        user7.setName("USER7");
        user7.setPassword(password);

        String baseUrl = webHelper.assembleBaseURL(request);

        userService.createUserV1(user1, baseUrl);
        userService.createUserV2(user2, baseUrl);
        userService.createUserV3(user3, baseUrl);
        userService.createUserV4(user4, baseUrl);


        modelMap.put("testusers", userService.getTestUsers(phone));

        return Constants.SIGNUP;

    }

    @RequestMapping(value = "/removetestusers")
    public String clean(ModelMap modelMap, HttpServletRequest request) {
        userService.removeTestUsers(phone);
        modelMap.put("testusers", userService.getTestUsers(phone));
        return Constants.SIGNUP;

    }


    @RequestMapping(value = "/joinfork")
    public String joinfork(ModelMap modelMap, HttpServletRequest request) {
        //Create ForkJoinPool using the default constructor.
        ForkJoinPool pool = new ForkJoinPool();

        String folder = "C:\\japp\\theautopark";
        //Create three FolderProcessor tasks. Initialize each one with a different folder path.
        FolderProcessor javaFiles = new FolderProcessor(folder, ".java");
        FolderProcessor xmlFiles = new FolderProcessor(folder, ".xml");
        FolderProcessor jspFiles = new FolderProcessor(folder, ".jsp");
        FolderProcessor classFiles = new FolderProcessor(folder, ".class");
        FolderProcessor jsFiles = new FolderProcessor(folder, ".js");
        FolderProcessor propFiles = new FolderProcessor(folder, ".properties");
        FolderProcessor pngFiles = new FolderProcessor(folder, ".png");
        FolderProcessor tagFiles = new FolderProcessor(folder, ".tag");
        FolderProcessor jarFiles = new FolderProcessor(folder, ".jar");
        FolderProcessor warFiles = new FolderProcessor(folder, ".war");
        FolderProcessor jsonFiles = new FolderProcessor(folder, ".json");
        FolderProcessor cssFiles = new FolderProcessor(folder, ".css");
        FolderProcessor htmlFiles = new FolderProcessor(folder, ".html");

        List<FolderProcessor> processors = Arrays.asList(
        javaFiles,xmlFiles,jspFiles,classFiles,jsFiles,propFiles,pngFiles,tagFiles,jarFiles,warFiles,jsonFiles,cssFiles,htmlFiles);


        //Execute the three tasks in the pool using the execute() method.
        for(FolderProcessor processor : processors){
            pool.execute(processor);
        }

        boolean work = true;
        while(work){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for(FolderProcessor processor : processors){
                if(!processor.isDone()){
                    continue;
                }
            }
            work = false;
        }

        //Shut down ForkJoinPool using the shutdown() method.
        pool.shutdown();

        Map<String,Integer> res = new HashMap<String, Integer>();
        for(FolderProcessor processor : processors){
            List<String> results = processor.join();
            res.put(processor.getExtension(),results!= null ? results.size() : 0);
        }

        modelMap.put("resMap", res);

        return Constants.SIGNUP;

    }

}
