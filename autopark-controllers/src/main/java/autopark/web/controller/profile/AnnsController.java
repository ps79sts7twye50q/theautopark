package autopark.web.controller.profile;

import autopark.domain.Role;
import autopark.dto.AnnDTO;
import autopark.service.IAnnService;
import autopark.web.auth.RequiresAuthentication;
import autopark.web.controller.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;


@Service
public class AnnsController {

    @Autowired
    private IAnnService annService;



    @RequestMapping(value = "/profile/anns")
    @RequiresAuthentication({Role.ROLE_USER})
    public String handle(ModelMap modelMap) {
        logger.debug("myanns");

        List<AnnDTO> list = annService.getMyAnns();

        modelMap.put(Constants.LIST,list);

        return "/WEB-INF/content/profile/myanns.jsp";
    }

}