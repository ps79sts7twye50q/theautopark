package autopark.web.controller.profile;

import autopark.domain.Role;
import autopark.dto.PrincipalUser;
import autopark.dto.UserDTO;
import autopark.service.IUserService;
import autopark.web.auth.RequiresAuthentication;
import autopark.web.controller.Constants;
import autopark.web.utils.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by aro on 07.03.2016.
 */
@Controller
public class ProfileController {
    private final static Logger logger = LoggerFactory.getLogger(ProfileController.class);

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    @RequestMapping(value = "/profile")
    @RequiresAuthentication({Role.ROLE_USER,Role.ROLE_ADMIN})
    public String handle(ModelMap modelMap) {
        logger.debug("Profile entered");

        UserDTO userDTO = userService.getUser();

        if(logger.isDebugEnabled()){
            logger.info("Profile entered, user={}",userDTO.getId());
        }

        modelMap.put(Constants.COMMAND,userDTO);
        return Constants.PROFILE;
    }


}
