package autopark.web.controller.car;

import autopark.dto.CommentDTO;
import autopark.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CarCommentController {

    @Autowired
    ICommentService commentService;

    @RequestMapping(value = "/comment/add")
    @ResponseBody
    public void handle(@ModelAttribute CommentDTO dto){
        commentService.createCarComment(dto);
    }


}



