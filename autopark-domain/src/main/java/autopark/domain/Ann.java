package autopark.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by  01 on 21.12.2015.
 */
@Entity
@Table(name = "ap_ann")
public class Ann extends Root {
    private String description;
    private User user;
    private Date creationDate;


    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public Date getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}