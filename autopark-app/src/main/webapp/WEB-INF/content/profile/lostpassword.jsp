<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="../tiles/head.jsp" %>
    <link rel="stylesheet" type="text/css" href="/styles/lostpassword.css" />
</head>
<body>
<tags:headerV2 activeProfile="active"/>
    <div id="wrap">

        <div class="container">
            <springForm:form action="/lostpassword" method="post" commandName="command" cssClass="form-lostpass">
                <h2 class="form-signin-heading">Восстановление пароля</h2>

                <input name="email" type="email" placeholder="Введите Email,используемый при регистрации" class="form-control" value="${command.email}" >
                <div><springForm:errors path="email" cssClass="error red" /></div>

                <button class="btn btn-lg btn-primary btn-block" type="submit">ОК</button>

            </springForm:form>
        </div>
    </div>






<hr>
<%@ include file="../tiles/footer.jsp" %>


</body>
</html>