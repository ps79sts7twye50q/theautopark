package autopark.dao.impl;

import autopark.dao.IAnnDAO;
import autopark.dao.ICarDAO;
import autopark.domain.*;
import autopark.dto.CarDTO;
import autopark.dto.CarSearchDTO;
import autopark.dto.PriceDTO;
import autopark.dto.UserDTO;
import autopark.rawconnection.Connections;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class AnnDAOImpl extends RootDAOImpl<Ann> implements IAnnDAO {


    public AnnDAOImpl() {
        super("autopark.domain.Ann", Ann.class);
    }

    public List<Ann> getCarsHibernate() {
        return findAllList();
    }

    }