package autopark.dao;


import autopark.domain.Demand;
import autopark.dto.DemandDTO;
import autopark.dto.DemandSearchDTO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IDemandDAO extends IRootDAO<Demand> {
    List<DemandDTO> searchDemands(DemandSearchDTO searchDTO);
    List<Demand> getDemands(Long userId);

}