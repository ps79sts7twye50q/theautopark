package autopark.dao;

import java.util.List;

/**
 * 10.04.2016.
 */
public interface GenericDAO<K, E> {
    boolean add(E entity);

    void saveOrUpdate(E entity);

    void update(E entity);

    void remove(E entity);

    E find(K key);

    List<E> getAll();
}
