package autopark.service.impl;

import autopark.dao.IAnnDAO;
import autopark.dao.ICommentDAO;
import autopark.dao.IUserDAO;
import autopark.domain.*;
import autopark.dto.AnnDTO;
import autopark.dto.CommentDTO;
import autopark.dto.DemandDTO;
import autopark.dto.PrincipalUser;
import autopark.service.IAnnService;
import autopark.service.ICommentService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 21.03.2016.
 */

@Service
public class AnnServiceImpl implements IAnnService {
    private final Logger log = LoggerFactory.getLogger(AnnServiceImpl.class);


    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private IAnnDAO annDAO;

    public List<AnnDTO> getMyAnns() {
        PrincipalUser principalUser = (PrincipalUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principalUser == null){
            return null;
        }

        User user = userDAO.getByEmail(principalUser.getUsername());

        if(user == null){
            return null;
        }
        List<AnnDTO> res = new ArrayList<AnnDTO>();

        List<Ann> anns = annDAO.getMyAnns(user.getId());
        if(anns != null){
            for(Demand demand: demands){
                res.add(DTOAssembler.assembleDemandDTO(demand));
            }
        }
        return res;
    }
}
