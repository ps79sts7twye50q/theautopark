package autopark.service.impl;

import autopark.dao.IDemandDAO;
import autopark.dao.IUserDAO;
import autopark.domain.Demand;
import autopark.domain.User;
import autopark.dto.DemandDTO;
import autopark.dto.DemandSearchDTO;
import autopark.dto.PrincipalUser;
import autopark.service.IDemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  01 on 15.02.2016.
 */
@Service
public class DemandServiceImpl implements IDemandService {

    @Autowired
    private IDemandDAO demandDAO;

    @Autowired
    private IUserDAO userDAO;

    public List<DemandDTO> searchDemands(DemandSearchDTO searchDTO) {
        return demandDAO.searchDemands(searchDTO);
    }

    @Override
    public List<DemandDTO> getDemands() {
        List<DemandDTO> res = new ArrayList<DemandDTO>();

        List<Demand> demands = demandDAO.findAllList();
        if(demands != null){
            for(Demand demand: demands){
                res.add(DTOAssembler.assembleDemandDTO(demand));
            }
        }
        return res;
    }

    @Override
    public List<DemandDTO> getMyDemands() {
        PrincipalUser principalUser = (PrincipalUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principalUser == null){
            return null;
        }

        User user = userDAO.getByEmail(principalUser.getUsername());

        if(user == null){
            return null;
        }
        List<DemandDTO> res = new ArrayList<DemandDTO>();

        List<Demand> demands = demandDAO.getDemands(user.getId());
        if(demands != null){
            for(Demand demand: demands){
                res.add(DTOAssembler.assembleDemandDTO(demand));
            }
        }
        return res;
    }



}
