package autopark.service;

import autopark.domain.Ann;
import autopark.dto.AnnDTO;
import autopark.dto.DemandDTO;
import autopark.dto.DemandSearchDTO;

import java.util.List;

public interface IAnnService {


    List<AnnDTO> getMyAnns();
}
