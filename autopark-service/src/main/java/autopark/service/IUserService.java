package autopark.service;

import autopark.domain.User;
import autopark.dto.PrincipalUser;
import autopark.dto.UserDTO;

import java.util.List;

public interface IUserService {
    boolean createUserV1(final UserDTO dto, final String baseURL);
    boolean createUserV2(final UserDTO dto, final String baseURL);
    boolean createUserV3(final UserDTO dto, final String baseURL);
    boolean createUserV4(final UserDTO dto, final String baseURL);
    boolean createUserV5(final UserDTO dto, final String baseURL);


    UserDTO getUserByEmail(String email);

    /**
     * Метод принимает информацию о нашем сервере, email
     * Достает пользователя, создает ссылку, и посылает на этот email ссылку.
     * @param ourServer - server info
     * @param email - email пользователя
     * @return
     */
    boolean processLostPassword(String ourServer,String email);

    boolean updateUserPassword(UserDTO userDTO, String baseURL);

    PrincipalUser assemblePrincipalUser(User user, Boolean facebook);

    UserDTO getUser();

    User getCurrentUser() throws AutoparkServiceException;

    List<UserDTO> getTestUsers(String phone);
    void removeTestUsers(String phone);

}
