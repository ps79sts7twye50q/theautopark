package autopark.service;

/**
 * Created by aro on 27.03.2016.
 */
public class AutoparkServiceException extends Exception {
    public AutoparkServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public AutoparkServiceException(String message) {
        super(message);
    }
}
